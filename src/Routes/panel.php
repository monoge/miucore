<?php
// Namespace: MiuCore\Controllers\Web

Route::get('/','HomeController@index')->name('home');

Route::get('{any}.xhtml', 'RouteController@staticPages')->where('any', '.*')->name('static');