<?php

namespace MiuCore\Controllers;

use MiuCore\Middleware\Panel\Authenticate;
use MiuCore\Middleware\Web\Loader;
use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{

    protected $routeMiddleware = [
        'panel.auth' => Authenticate::class,
        'web.loader' => Loader::class,
    ];

}
