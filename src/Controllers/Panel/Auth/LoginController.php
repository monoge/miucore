<?php

namespace MiuCore\Controllers\Web\Auth;

use MiuCore\Controllers\Panel\Controller;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
//    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function showLoginForm()
    {
        return view('core::web.auth.login');
    }

    protected function validateLogin(Request $request)
    {
        $request = $request->all();

        $validate = Validator::make($request, [
            $this->username() => 'required|string|min:6|max:255',
            'password' => 'required|string|min:6|max:50',
        ]);

        if($validate->fails()){
            return response()->json(trans('auth.failed'),422);
        }
    }

    protected function sendFailedLoginResponse(Request $request)
    {
        return response()->json(trans('auth.failed'),422);
    }

    protected function authenticated(Request $request, $user)
    {
        return redirect()->route('web.home');
        if($user->status!=1){
            return $this->logoutJson($request);
        }

        return response()->json($redirect,200);
    }

    public function logoutJson(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return response()->json(trans('auth.failed'),422);
    }

    public function logoutCustom($monster)
    {
        return $this->logout($monster->request);
    }

    public function username()
    {
        return 'email';
    }

}
