<?php

namespace MiuCore\Controllers\Panel;

use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    public function index()
    {
//        $admin = \Auth::guard('admin')->attempt(['email' => 'info@mail.com', 'password' => '123123ab']);
//        return redirect(url('/panel'));
//        dd(uniqid());
        return view('miucore::panel.index');
    }
}
