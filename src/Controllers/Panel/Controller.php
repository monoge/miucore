<?php

namespace MiuCore\Controllers\Panel;

use MiuCore\Models\Config;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $query = \Cache::rememberForever('configs',function(){
            return Config::select(\DB::raw("CONCAT(type,'.',code) as code"),'value')->pluck('value','code')->toArray();
        });
        config($query);
    }
}
