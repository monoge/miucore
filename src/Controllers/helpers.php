<?php

if ( !function_exists('assets') )
{
    function assets($text='/')
    {
        return config('app.url') . ( $text == '/' ? null : '/assets/' ) . $text;
    }
}

function web($text='/')
{
    return config('app.url') . ( $text == '/' ? null : '/assets/' ) . $text;
}

function urlsRoute( $key = null )
{
    $url = \MiuCore\Models\Url::where('type','static')
        ->where('key',$key)->orderByDesc('updated_at')->first();

    return $url ? flaggable($url->slug) : null;
}

function flaggable( $url = '/' )
{
    return config('app.url') . '/' . $url;
}