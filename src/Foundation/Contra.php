<?php

namespace MiuCore\Foundation;

use MiuCore\Models\Website;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class Contra
{
    public $request;
    public $website;
    public $domain;
    public $cacheKey;

    public function __construct()
    {
        $this->request = Request::capture();
        $this->website  = $this->website();
        $this->user = auth()->guest() ? null : auth()->user();

        return $this;
    }

    public function website()
    {
        $server_host = str_replace(['http://','https://'],'',$this->request->server->get('HTTP_HOST'));

        $slug[0] = str_replace('www.','',$server_host);
        $slug[1] = 'www.'.current($slug);

        $this->cacheKey = $cacheKey = implode('|',$slug);
        $cachePrefix = md5($cacheKey);

        if( env('CACHE_DRIVER','file') == 'redis' ){
            Cache::setPrefix($cachePrefix);
        }

        $website = Cache::rememberForever($cacheKey,function()use($slug){

            $query = Website::with(['currency' => function($q){
                    $q->where('status',1);
                }])
                ->whereIn('slug',$slug)
                ->where('type','domain')
                ->where('status',1)
                ->first();

            if ( !$query ) {
                $query = Website::with(['currency' => function($q){
                        $q->where('status',1);
                    }])
                    ->whereIn('slug',$slug)
                    ->where('type','alias')
                    ->where('status',1)
                    ->firstOrFail();
                $query->id = $query->website_id;
            }

            $query->domain = ( $query->ssl == 1 ? 'https://' : 'http://' ) . $query->slug;

            return $query;

        });

        if (
            $server_host != $website->slug ||
            $website->ssl == 1 && $this->request->getScheme() == 'http' ||
            $website->ssl == 0 && $this->request->getScheme() == 'https'
        ) {
            throw new RedirectException( $website->slug . $this->request->getRequestUri(), 301 );
        }

        config([
            'app.url' => $website->domain,
            'app.cachePrefix' => $cachePrefix,
        ]);

        $this->domain = $website->domain;

        return $website;
    }

    public static function decrypt( $value = null, $ajax = true )
    {
        try {
            $value = decrypt($value);
        } catch (\Illuminate\Contracts\Encryption\DecryptException $e) {
            return $ajax ? false : abort(404);
        }

        return $value;
    }

}
