<?php

namespace MiuCore\Console\Setup;

use Illuminate\Console\Command;

class Seed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'miu:seed {website=1}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Veritabanı kurulum işlemleri';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $website = $this->argument('website');

        config(['miucore.seed.website_id'=>$website]);

        \Artisan::call('db:seed', [ '--class'=> 'MiuCore\\Database\\Seeds\\DatabaseSeeder' ]);
        $this->info('Seed    : OK');

        \Artisan::call('cache:clear');
        $this->comment('Başarılı !');
    }

}
