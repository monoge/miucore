<?php

namespace MiuCore\Console\Setup;

use Illuminate\Console\Command;

class My extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'miu:my-migrate {operation=default}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'My Veritabanı kurulum işlemleri';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Artisan::call('migrate:fresh', [
            '--path'    => 'vendor/monoge/miucore/src/Database/Migrations/My',
        ]);

        $this->info('Migrate : OK');

        $operation = $this->argument('operation');

        if ( $operation == 'seed' ) {

            \Artisan::call('db:seed', [
                '--class'   => 'MiuCore\\Database\\Seeds\\My\\DatabaseSeeder'
            ]);

            $this->info('Seed    : OK');

        }

        \Artisan::call('cache:clear');

        $this->comment('Başarılı !');
    }

}
