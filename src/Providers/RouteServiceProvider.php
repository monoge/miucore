<?php

namespace MiuCore\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    protected $apiNamespace = 'MiuCore\Controllers\Api';
    protected $thirdNamespace = 'MiuCore\Controllers\Third';
    protected $panelNamespace = 'MiuCore\Controllers\Panel';

    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();
        $this->mapThirdRoutes();
        $this->mapPanelRoutes();
    }

    protected function mapApiRoutes()
    {
        Route::name('api.')
            ->prefix('api')
            ->middleware('api')
            ->namespace($this->apiNamespace)
            ->group(__DIR__.'/../Routes/api.php');
    }

    protected function mapThirdRoutes()
    {
        Route::name('third.')
            ->prefix('third')
            ->namespace($this->thirdNamespace)
            ->group(__DIR__.'/../Routes/third.php');
    }

    protected function mapPanelRoutes()
    {
        Route::name('panel.')
            ->middleware('web')
            ->namespace($this->panelNamespace)
            ->group(__DIR__.'/../Routes/panel.php');
    }
}
