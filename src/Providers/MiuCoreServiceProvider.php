<?php

namespace MiuCore\Providers;

use MiuCore\Console\Setup\My;
use MiuCore\Console\Setup\Seed;
use MiuCore\Controllers\Kernel;
use MiuCore\Middleware\Loader;
use Illuminate\Contracts\Http\Kernel as HttpKernel;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class MiuCoreServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->init();
        $this->loaders();

        if ( $this->app->runningInConsole() ) {
            /*
            if ( $this->isConsoleCommandContains(['db:seed','--seed'],['--class','help','-h']) ) {

            }
            */
            $this->commands([
                My::class,
                Seed::class
            ]);
        } else {
        }

    }

    protected function init()
    {
        App::setLocale('tr');
    }

    protected function loaders()
    {
        $this->loadTranslationsFrom(__DIR__.'/../Translations', 'miucore');
        $this->loadViewsFrom(__DIR__.'/../Resources/views', 'miucore');

        $this->publishes([
            base_path('vendor/monoge/miucore/src/Resources/assets') => public_path('assets'),
//            base_path('vendor/monoge/miucore/src/Resources/themes') => resource_path('views'),
        ], 'miucore');
    }

    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../Config/auth.php', 'auth'
        );

        require_once(__DIR__ . '/../Controllers/helpers.php');

        $this->app->singleton(HttpKernel::class, Kernel::class);
//        $this->app->bind('Contra', 'MiuCore\Foundation\Contra');

        $this->app->register(RouteServiceProvider::class);

        $loader = AliasLoader::getInstance();
//        $loader->alias('Helper',Helper::class);
    }

    protected function mergeConfigFrom($path, $key)
    {
        $config = $this->app['config']->get($key, []);

        foreach (require $path as $k => $v) {
            if (is_array($v)) {
                if (isset($config[$k])) {
                    $config[$k] = array_merge($config[$k], $v);
                } else {
                    $config[$k] = $v;
                }

            } else {
                $config[$k] = $v;
            }
        }
        $this->app['config']->set($key, $config);
    }


}
