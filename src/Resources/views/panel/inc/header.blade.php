<div id="header">
  <nav class="navbar navbar-expand-lg navbar-light bg-light py-1">
    <a class="navbar-brand font-weight-bold text-light" href="#">E-TİCARET</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="index.html">Giriş Ekranı</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Siparişler</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#"role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Katalog
          </a>
          <div class="dropdown-menu">
            <a class="dropdown-item" href="products.html">Ürün Yönetimi</a>
            <a class="dropdown-item" href="products.html">Kategori Yönetimi</a>
            <a class="dropdown-item" href="products.html">Marka Yönetimi</a>
            <a class="dropdown-item" href="products.html">Etiket Yönetimi</a>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Aktarımlar</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Entegrasyonlar</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Modüller</a>
        </li>
      </ul>
    </div>
  </nav>
</div>