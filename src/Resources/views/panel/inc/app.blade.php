<!doctype html>
<html lang="{{App::getLocale()}}">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{assets('panel/style.css')}}">
{{--    <link rel="stylesheet" href="{{Helper::asset('login/style.css')}}">--}}
    <title>{{env('TITLE')}}</title>
  </head>
  <body>
    @include('miucore::panel.inc.header')
    <div id="content">
      <div class="container-fluid p-3">
        @yield('content')
      </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
  </body>
</html>
