@extends('miucore::panel.inc.app')

@section('content')
    <div class="row">

        <div class="col-md-2">
            <a href="#" class="card">
                <div class="card-body">
                    <p class="text-center pt-3"><img src="{{assets('panel/img/icons/orders.png')}}"></p>
                    <h5 class="card-title text-center font-weight-bold pt-2">Siparişler</h5>
                    <p class="card-text">Satış kanallarınızdan gelen siparişlerin tümünü görebilirsiniz.</p>
                </div>
            </a>
        </div>

        <div class="col-md-2">
            <a href="#" class="card">
                <div class="card-body">
                    <p class="text-center pt-3"><img src="{{assets('panel/img/icons/products.png')}}"></p>
                    <h5 class="card-title text-center font-weight-bold pt-2">Ürünler</h5>
                    <p class="card-text">Ürünlerinizin listesini ve yönetimini bu alandan görebilirsiniz.</p>
                </div>
            </a>
        </div>

        <div class="col-md-2">
            <a href="#" class="card">
                <div class="card-body">
                    <p class="text-center pt-3"><img src="{{assets('panel/img/icons/users.png')}}"></p>
                    <h5 class="card-title text-center font-weight-bold pt-2">Kullanıcılar</h5>
                    <p class="card-text">Uygulama üzerinde kullanıcılar oluşturarak, yetkilendirebilirsiniz.</p>
                </div>
            </a>
        </div>

        <div class="col-md-2">
            <a href="#" class="card">
                <div class="card-body">
                    <p class="text-center pt-3"><img src="{{assets('panel/img/icons/transfers.png')}}"></p>
                    <h5 class="card-title text-center font-weight-bold pt-2">Aktarımlar</h5>
                    <p class="card-text">Uygulama üzerinde bulunan tüm verileri iç/dış aktarabilirsiniz.</p>
                </div>
            </a>
        </div>

        <div class="col-md-2">
            <a href="#" class="card">
                <div class="card-body">
                    <p class="text-center pt-3"><img src="{{assets('panel/img/icons/integrations.png')}}"></p>
                    <h5 class="card-title text-center font-weight-bold pt-2">Entegrasyonlar</h5>
                    <p class="card-text">Entegrasyon sağlayabileceğiniz tüm detayları yönetebilirsiniz.</p>
                </div>
            </a>
        </div>

    </div>

@endsection