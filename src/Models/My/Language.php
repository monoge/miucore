<?php

namespace MiuCore\Models\My;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Language extends Model
{
    use SoftDeletes;
    protected $guarded = ['created_at','updated_at','deleted_at'];
}
