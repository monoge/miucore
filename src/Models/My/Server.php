<?php

namespace MiuCore\Models\My;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Server extends Model
{
    use SoftDeletes;
    protected $guarded = [ 'id','created_at','updated_at','deleted_at'];
}
