<?php

namespace MiuCore\Models\Remote;

use Illuminate\Database\Eloquent\SoftDeletes;

class Url extends BaseModel
{
    use SoftDeletes;
    protected $connection = 'remote';
    protected $guarded = ['created_at','updated_at','deleted_at'];

    public function details()
    {
        return $this->hasMany(UrlDetail::class);
    }
}
