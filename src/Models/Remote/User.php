<?php

namespace MiuCore\Models\Remote;

use Illuminate\Database\Eloquent\SoftDeletes;

class User extends BaseModel
{
    use Notifiable;
    use SoftDeletes;

    protected $connection = 'remote';
    protected $guarded = ['created_at','updated_at','deleted_at'];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getFullNameAttribute()
    {
        return "{$this->name} {$this->surname}";
    }

    public function getBirthAttribute()
    {
        $date = $this->birthdate;

        if(!$date){ return collect(); }

        $date = array_reverse( explode('-',$date) );

        return collect([
            'day'   => $date[0],
            'month' => $date[1],
            'year'  => $date[2],
        ]);
    }

    public function group()
    {
        return $this->belongsTo(UserGroup::class,'user_group_id','id');
    }
}
