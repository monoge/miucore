<?php

namespace MiuCore\Models\Remote;

class UserExtra extends BaseModel
{
    public $timestamps = false;
    protected $connection = 'remote';
    protected $guarded = [];
}
