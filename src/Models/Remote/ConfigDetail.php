<?php

namespace MiuCore\Models\Remote;

class ConfigDetail extends BaseModel
{
    public $timestamps = false;
    protected $guarded = [];

    protected $casts = [
        'parameters' => 'array',
    ];

    public function parent()
    {
        return $this->belongsTo(Config::class,'config_id','id');
    }
}
