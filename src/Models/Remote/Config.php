<?php

namespace MiuCore\Models\Remote;

use Illuminate\Database\Eloquent\SoftDeletes;

class Config extends BaseModel
{
    use SoftDeletes;
    protected $connection = 'remote';
    protected $guarded  = ['created_at','updated_at','deleted_at'];

    protected $casts = [
        'parameters' => 'array',
    ];

    public function detail()
    {
        return $this->hasOne(ConfigDetail::class);
    }
}
