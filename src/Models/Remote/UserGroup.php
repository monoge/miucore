<?php

namespace MiuCore\Models\Remote;

use Illuminate\Database\Eloquent\SoftDeletes;

class UserGroup extends BaseModel
{
    use SoftDeletes;
    protected $connection = 'remote';
    protected $guarded = ['created_at','updated_at','deleted_at'];

    public function users()
    {
        return $this->hasMany(User::class);
    }
}
