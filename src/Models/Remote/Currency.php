<?php

namespace MiuCore\Models\Remote;

use Illuminate\Database\Eloquent\SoftDeletes;

class Currency extends BaseModel
{
    use SoftDeletes;
    protected $connection = 'remote';
    protected $guarded = [ 'id','created_at','updated_at','deleted_at'];
}
