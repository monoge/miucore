<?php

return [

    'guards' => [
//        'admin' => [
//            'driver' => 'session',
//            'provider' => 'admins',
//        ],
        'user' => [
            'driver' => 'session',
            'provider' => 'users',
        ],
    ],

    'providers' => [
//        'admins' => [
//            'driver' => 'eloquent',
//            'table' => 'admins',
//            'model' => \MiuCore\Models\Admin::class,
//        ],
        'users' => [
            'driver' => 'eloquent',
            'table' => 'users',
            'model' => \MiuCore\Models\User::class,
        ],
    ],

];