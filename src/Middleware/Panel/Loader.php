<?php

namespace MiuCore\Middleware\Panel;

use Closure;

class Loader
{
    public function handle($request, Closure $next)
    {
        return $next($request);
    }
}
