<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('remote')->create('users', function (Blueprint $table) {

            $table->bigIncrements('id');

            $table->integer('project_id')->unsigned()->default(1);
            $table->integer('user_group_id')->unsigned()->default(1)->index();
            $table->integer('language_id')->unsigned()->index();

            $table->string('code')->nullable()->unique();

            $table->string('name',50)->nullable();
            $table->string('surname',50)->nullable();

            $table->string('identity_number',30)->nullable();
            $table->enum('gender',['none','man','woman'])->nullable();
            $table->date('birthdate')->nullable();

            $table->string('phone',25)->nullable();

            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();

            $table->tinyInteger('status')->default(1);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('remote')->dropIfExists('users');
    }
}
