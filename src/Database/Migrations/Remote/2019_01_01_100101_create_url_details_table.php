<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUrlDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('remote')->create('url_details', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('url_id')->index();
            $table->integer('language_id')->index();
            $table->integer('model_id')->default(0);

            $table->string('slug')->index();

            $table->tinyInteger('status')->default(0)->index();

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('remote')->dropIfExists('url_details');
    }
}
