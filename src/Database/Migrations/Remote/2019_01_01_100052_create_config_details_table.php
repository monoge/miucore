<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('remote')->create('config_details', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('config_id')->unsigned()->index();
            $table->integer('language_id')->unsigned()->index();

            $table->string('name',255);
            $table->string('description',255)->nullable();

            $table->json('parameters');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('remote')->dropIfExists('config_details');
    }
}
