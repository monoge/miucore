<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('remote')->create('configs', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('project_id')->unsigned()->index();

            $table->string('type',100)->default('type');

            $table->string('code',100)->default('code');
            $table->string('value',100)->nullable();

            $table->json('parameters');
            $table->string('column_type',100)->default('input');

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('remote')->dropIfExists('configs');
    }
}
