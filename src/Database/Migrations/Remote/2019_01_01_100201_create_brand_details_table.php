<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrandDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('remote')->create('brand_details', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('language_id')->unsigned()->index();
            $table->integer('brand_id')->unsigned()->index();

            $table->string('name');
            $table->string('slug')->index();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('remote')->dropIfExists('brand_details');
    }
}
