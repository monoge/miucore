<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCurrenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('remote')->create('currencies', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('currency_id')->unsigned()->index(); // My/Currencies.id

            $table->enum('direction',['left','right']);

            $table->enum('space',[1,0]);
            $table->enum('show',['define','symbol']);

            $table->tinyInteger('decimal_section')->nullable()->default(1);

            $table->string('decimal_place',2)->nullable()->default('.');
            $table->string('thousandths_place',2)->nullable()->default(',');

            $table->tinyInteger('unit')->nullable()->default(1);

            $table->decimal('value',18,4)->default(0);
            $table->tinyInteger('value_rate')->nullable()->default(0);

//            $table->enum('tcmb_update',[0,1])->default('0');

            $table->tinyInteger('status')->nullable()->default(0);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('remote')->dropIfExists('currencies');
    }
}
