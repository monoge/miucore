<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('remote')->create('user_groups', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('project_id')->unsigned()->default(1)->index();
            $table->integer('user_group_id')->unsigned()->default(1);

            $table->string('name',100)->nullable();

            $table->tinyInteger('default')->default(0);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('remote')->dropIfExists('user_groups');
    }
}
