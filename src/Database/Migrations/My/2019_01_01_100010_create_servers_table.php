<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servers', function (Blueprint $table) {

            $table->increments('id');

            $table->enum('type',['web','storage','database','redis']);

            $table->string('name',50)->nullable();
            $table->longText('detail')->nullable();


            $table->string('ip',50);
            $table->string('domain',50);

            $table->integer('port');

            $table->string('login',50)->nullable();
            $table->string('password',50)->nullable();

            $table->integer('limit')->default(0);

            $table->integer('order')->default(0);
            $table->enum('status',['service','active','passive']);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servers');
    }
}
