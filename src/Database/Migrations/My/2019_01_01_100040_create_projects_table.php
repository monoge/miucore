<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {

            $table->increments('id');

            $table->integer('app_id')->unsigned()->default(1)->index();
/*
            $table->integer('file_server_id')->unsigned();
            $table->integer('db_server_id')->unsigned();
            $table->integer('storage_server_id')->unsigned()->nullable();
*/
            $table->string('code',50)->nullable();

            $table->string('domain')->nullable();

            $table->integer('language_id')->unsigned()->default(1);
            $table->integer('currency_id')->unsigned()->default(1);

            $table->enum('ssl', array('0', '1'));
            $table->enum('type', array('domain', 'alias'));
            $table->integer('project_id')->default(0);

            $table->json('data')->nullable();

            $table->tinyInteger('status')->nullable()->default(0);

            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
}
