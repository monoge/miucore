<?php

namespace MiuCore\Database\Seeds\Remote;

use MiuCore\Controllers\Web\Auth\LoginController;
use MiuCore\Controllers\Web\HomeController;
use MiuCore\Models\Remote\Url;
use MiuCore\Models\Remote\UrlDetail;
use Illuminate\Database\Seeder;

class UrlTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $list = [
            [
                'type' => 'static', 'key' => 'home', 'controller' => HomeController::class, 'method' => 'index',
                'languages' => [
                    [ 'language_id' => 1, 'model_id' => 0, 'slug' => '/', 'status' => 1 ],
                    [ 'language_id' => 2, 'model_id' => 0, 'slug' => '/en', 'status' => 1 ],
                    [ 'language_id' => 3, 'model_id' => 0, 'slug' => '/de', 'status' => 1 ],
                ],
            ],
            [
                'type' => 'static', 'key' => 'auth.login', 'controller' => LoginController::class, 'method' => 'showLoginForm',
                'languages' => [
                    [ 'language_id' => 1, 'model_id' => 0, 'slug' => '/giris', 'status' => 1 ],
                    [ 'language_id' => 2, 'model_id' => 0, 'slug' => '/login', 'status' => 1 ],
                    [ 'language_id' => 3, 'model_id' => 0, 'slug' => '/einloggen', 'status' => 1 ],
                ],
            ],
            [
                'type' => 'static', 'key' => 'auth.register', 'controller' => null, 'method' => 'showRegistrationForm',
                'languages' => [
                    [ 'language_id' => 1, 'model_id' => 0, 'slug' => '/kayit-ol', 'status' => 1 ],
                    [ 'language_id' => 2, 'model_id' => 0, 'slug' => '/register', 'status' => 1 ],
                    [ 'language_id' => 3, 'model_id' => 0, 'slug' => '/registrieren', 'status' => 1 ],
                ],
            ],
            [
                'type' => 'static', 'key' => 'auth.forgot-password', 'controller' => null, 'method' => 'showLinkRequestForm',
                'languages' => [
                    [ 'language_id' => 1, 'model_id' => 0, 'slug' => '/sifremi-unuttum', 'status' => 1 ],
                    [ 'language_id' => 2, 'model_id' => 0, 'slug' => '/forgot-my-password', 'status' => 1 ],
                    [ 'language_id' => 3, 'model_id' => 0, 'slug' => '/passwort-vergessen', 'status' => 1 ],
                ],
            ],
            [
                'type' => 'static', 'key' => 'auth.reset-password', 'controller' => null, 'method' => 'showResetForm',
                'languages' => [
                    [ 'language_id' => 1, 'model_id' => 0, 'slug' => '/sifremi-sifirla', 'status' => 1 ],
                    [ 'language_id' => 2, 'model_id' => 0, 'slug' => '/reset-my-password', 'status' => 1 ],
                    [ 'language_id' => 3, 'model_id' => 0, 'slug' => '/setze-mein-passwort-zuruck', 'status' => 1 ],
                ],
            ],
            [
                'type' => 'static', 'key' => 'auth.logout', 'controller' => null, 'method' => 'logoutCustom',
                'languages' => [
                    [ 'language_id' => 1, 'model_id' => 0, 'slug' => '/cikis', 'status' => 1 ],
                    [ 'language_id' => 2, 'model_id' => 0, 'slug' => '/logout', 'status' => 1 ],
                    [ 'language_id' => 3, 'model_id' => 0, 'slug' => '/ausloggen', 'status' => 1 ],
                ],
            ],
            [
                'type' => 'static', 'key' => 'account.index', 'controller' => null, 'method' => 'index',
                'languages' => [
                    [ 'language_id' => 1, 'model_id' => 0, 'slug' => '/hesabim', 'status' => 1 ],
                    [ 'language_id' => 2, 'model_id' => 0, 'slug' => '/my-account', 'status' => 1 ],
                    [ 'language_id' => 3, 'model_id' => 0, 'slug' => '/mein-konto', 'status' => 1 ],
                ],
            ],
            [
                'type' => 'static', 'key' => 'account.address', 'controller' => null, 'method' => 'index',
                'languages' => [
                    [ 'language_id' => 1, 'model_id' => 0, 'slug' => '/hesabim/adreslerim', 'status' => 1 ],
                    [ 'language_id' => 2, 'model_id' => 0, 'slug' => '/my-account/address', 'status' => 1 ],
                    [ 'language_id' => 3, 'model_id' => 0, 'slug' => '/mein-konto/addresse', 'status' => 1 ],
                ],
            ],
            [
                'type' => 'static', 'key' => 'account.change-password', 'controller' => null, 'method' => 'index',
                'languages' => [
                    [ 'language_id' => 1, 'model_id' => 0, 'slug' => '/hesabim/sifre-degistir', 'status' => 1 ],
                    [ 'language_id' => 2, 'model_id' => 0, 'slug' => '/my-account/change-password', 'status' => 1 ],
                    [ 'language_id' => 3, 'model_id' => 0, 'slug' => '/mein-konto/andere-das-passwort', 'status' => 1 ],
                ],
            ],
            [
                'type' => 'static', 'key' => 'account.orders', 'controller' => null, 'method' => 'index',
                'languages' => [
                    [ 'language_id' => 1, 'model_id' => 0, 'slug' => '/hesabim/siparislerim', 'status' => 1 ],
                    [ 'language_id' => 2, 'model_id' => 0, 'slug' => '/my-account/orders', 'status' => 1 ],
                    [ 'language_id' => 3, 'model_id' => 0, 'slug' => '/mein-konto/anweisung', 'status' => 1 ],
                ],
            ],
        ];

        foreach ( $list as $li ) {

            $row = Url::create([
                'type' => $li['type'],
                'key' => $li['key'],
                'controller' => $li['controller'],
                'method' => $li['method'],
            ]);

            foreach ( $li['languages'] as $lang ) {

                UrlDetail::insert([
                    'url_id' => $row->id,
                    'language_id' => $lang['language_id'],
                    'model_id' => $lang['model_id'],
                    'slug' => $lang['slug'],
                    'status' => $lang['status'],
                ]);

            }

        }

        dump('# URL\'ler yüklendi.');

    }
}
