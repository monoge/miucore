<?php

namespace MiuCore\Database\Seeds\Remote;

use MiuCore\Database\Seeds\Remote\ConfigTableSeeder;
use MiuCore\Models\User;
use MiuCore\Models\UserGroup;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ConfigTableSeeder::class);
        $this->call(CurrencyTableSeeder::class);
        $this->call(UrlTableSeeder::class);

        // Müşteri grubu & Admin
        $user_group = UserGroup::create([
//            'website_id' => config('miucore.seed.website_id'),
            'name' => 'Standart Müşteri',
            'default' => 1,
        ]);

        User::create([
            'user_group_id' => $user_group->id,
            'name' => 'Melek',
            'surname' => 'Çetin',
            'email' => 'melek@mail.com',
            'password' => bcrypt('123123ab'),
        ]);

    }
}
