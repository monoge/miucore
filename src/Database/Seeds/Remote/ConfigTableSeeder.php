<?php

namespace MiuCore\Database\Seeds\Remote;

use MiuCore\Models\Remote\Config;
use MiuCore\Models\Remote\ConfigDetail;
use Illuminate\Database\Seeder;

class ConfigTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        $list = [
            [ // Ürünler
                'type' => 'products', 'code' => 'non-stock', 'value' => 'show', 'parameters' => ['show','hide'], 'column_type' => 'select',
                'name' => 'Stoğu Olmayan Ürünler', 'description' => 'Stoğu olmayan ürünleri göster/gizle aksiyonu içindir.',
                'parameters.language' => ['show' => 'Göster', 'hide' => 'Gizle'],
            ],
            [
                'type' => 'products', 'code' => 'default_image', 'value' => 'assets/constant/not-found.jpg', 'parameters' => [], 'column_type' => 'image_upload',
                'name' => 'Varsayılan Ürün Resmi', 'description' => 'Eklenmiş olan ürünler için resim yüklenmediyse bu resim görünecektir.',
                'parameters.language' => [],
            ],
            [
                'type' => 'products', 'code' => 'default_tax', 'value' => '18', 'parameters' => [], 'column_type' => 'input',
                'name' => 'Varsayılan Vergi Miktarı', 'description' => 'Ürün eklenirken, Vergi Miktarı bölümünde otomatik olarak gelen değer.',
                'parameters.language' => [],
            ],

            [ // Stok birimleri
                'type' => 'stock_units', 'code' => 'seperator', 'value' => ',', 'parameters' => ['.',','], 'column_type' => 'select',
                'name' => 'Stok Ondalık Ayracı', 'description' => 'Stok gösteriminde kullanılacak ondalık ayracıdır.',
                'parameters.language' => ['.' => 'Nokta', ',' => 'Virgül'],
            ],
        ];

        $i=1;
        foreach($list as $li){

            $row = Config::create([
                'website_id'  => config('miu.seed.website_id'),
                'type'  => $li['type'],
                'code'  => $li['code'],
                'value' => $li['value'],
                'parameters'    => $li['parameters'],
            ]);

            ConfigDetail::create([
                'config_id'     => $row->id,
                'language_id'   => config('m,u.seed.website_id'),
                'name'  => $li['name'],
                'description'   => $li['description'],
                'parameters'    => $li['parameters.language'],
            ]);

            $i++;
        }

        dump('# Ayarlar oluşturuldu.');

    }
}
