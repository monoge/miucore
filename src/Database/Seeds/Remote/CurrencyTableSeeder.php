<?php

namespace MiuCore\Database\Seeds\Remote;

use MiuCore\Models\Remote\Currency;
use Illuminate\Database\Seeder;

class CurrencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = [
            [
                'currency_id' => 1,
                'direction' => 'left',
                'space' => 1,
                'show' => 'symbol',
                'decimal_section' => 2,
                'decimal_place' => '.',
                'thousandths_place' => ',',
                'value' => 1,
                'status' => 1
            ],
            [
                'currency_id' => 2,
                'direction' => 'left',
                'space' => 1,
                'show' => 'symbol',
                'decimal_section' => 2,
                'decimal_place' => '.',
                'thousandths_place' => ',',
                'value' => 1,
                'status' => 1
            ],
            [
                'currency_id' => 3,
                'direction' => 'right',
                'space' => 1,
                'show' => 'define',
                'decimal_section' => 2,
                'decimal_place' => ',',
                'thousandths_place' => '.',
                'value' => 1,
                'status' => 1
            ],
            [
                'currency_id' => 4,
                'direction' => 'left',
                'space' => 1,
                'show' => 'symbol',
                'decimal_section' => 2,
                'decimal_place' => '.',
                'thousandths_place' => ',',
                'value' => 1,
                'status' => 1
            ],
            [
                'currency_id' => 5,
                'direction' => 'right',
                'space' => 1,
                'show' => 'symbol',
                'decimal_section' => 2,
                'decimal_place' => '.',
                'thousandths_place' => ',',
                'value' => 1,
                'status' => 1
            ]
        ];

        Currency::insert( $arr );

        dump('# Para birimleri oluşturuldu.');
    }
}
