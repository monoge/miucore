<?php

namespace MiuCore\Database\Seeds\My;

use MiuCore\Models\My\Currency;
use Illuminate\Database\Seeder;

class CurrencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr = [
            [
                'code'      => 'USD',
                'define'    => 'USD',
                'symbol'    => '$',
                'order'     => 1,
                'status'    => 1
            ],
            [
                'code'      => 'EUR',
                'define'    => 'EUR',
                'symbol'    => '€',
                'order'     => 2,
                'status'    => 1
            ],
            [
                'code'      => 'TRY',
                'define'    => 'TL',
                'symbol'    => '₺',
                'order'     => 3,
                'status'    => 1
            ],
            [
                'code'      => 'GBP',
                'define'    => 'GBP',
                'symbol'    => '£',
                'order'     => 4,
                'status'    => 0
            ],
            [
                'code'      => 'RUB',
                'define'    => 'RUB',
                'symbol'    => '₽',
                'order'     => 5,
                'status'    => 0
            ],
        ];

        Currency::insert( $arr );

        dump('# Para birimleri oluşturuldu.');
    }
}
