<?php

namespace MiuCore\Database\Seeds\My;

use MiuCore\Models\My\Locale;
use Illuminate\Database\Seeder;

class LocaleTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $arr = [
            [
                'code'  => 'tr',
                'name'  => 'Türkçe',
                'charset'   => 'UTF-8',
                'order'     => 1,
                'status'    => 1,
            ],
            [
                'code'  => 'en',
                'name'  => 'English',
                'charset'   => 'UTF-8',
                'order'     => 2,
                'status'    => 0,
            ],
            [
                'code'  => 'de',
                'name'  => 'Deutsch',
                'charset'   => 'UTF-8',
                'order'     => 3,
                'status'    => 0,
            ],
            [
                'code'  => 'fr',
                'name'  => 'Français',
                'charset'   => 'UTF-8',
                'order'     => 4,
                'status'    => 0,
            ],
            [
                'code'  => 'it',
                'name'  => 'Italiano',
                'charset'   => 'UTF-8',
                'order'     => 5,
                'status'    => 0,
            ],
        ];

        Locale::insert( $arr );

        dump('# Panel dilleri oluşturuldu.');
    }
}
