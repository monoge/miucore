<?php

namespace MiuCore\Database\Seeds\My;

use MiuCore\Models\My\Server;
use Illuminate\Database\Seeder;

class ServerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr    = [
            [
                'type'  => 'file',
                'slug'  => 'production1',
                'local_ip'  => '127.0.0.1',
                'public_ip' => '127.0.0.1',
                'domain'    => 'pr1.miura-project.com',
                'port'  => '22',
                'login' => 'demo',
                'password'  => 'demo',
                'limit' => '9999',
                'order' => '1',
                'status'    => '1',
            ],
            [
                'type'  => 'pqsql',
                'slug'  => 'database1',
                'local_ip'  => '127.0.0.1',
                'public_ip' => '127.0.0.1',
                'domain'    => 'db1.miura-project.com',
                'port'  => '22',
                'login' => 'demo',
                'password'  => 'demo',
                'limit' => '9999',
                'order' => '1',
                'status'    => '1',
            ],
            [
                'type'  => 'storage',
                'slug'  => 'storage1',
                'local_ip'  => '127.0.0.1',
                'public_ip' => '127.0.0.1',
                'domain'    => 'st1.miura-project.com',
                'port'  => '22',
                'login' => 'demo',
                'password'  => 'demo',
                'limit' => '9999',
                'order' => '1',
                'status'    => '1',
            ],
            [
                'type'  => 'storage',
                'slug'  => 'storage2',
                'local_ip'  => '127.0.0.1',
                'public_ip' => '127.0.0.1',
                'domain'    => 'st2.miura-project.com',
                'port'  => '22',
                'login' => 'demo',
                'password'  => 'demo',
                'limit' => '9999',
                'order' => '2',
                'status'    => '1',
            ]
        ];

        Server::insert( $arr );

        dump('# Server\'lar oluşturuldu.');
    }
}
