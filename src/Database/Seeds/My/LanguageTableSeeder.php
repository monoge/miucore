<?php

namespace MiuCore\Database\Seeds\My;

use MiuCore\Models\My\Language;
use Illuminate\Database\Seeder;

class LanguageTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $arr = [
            [
                'code'  => 'tr',
                'name'  => 'Türkçe',
                'charset'   => 'UTF-8',
                'order'     => 1,
                'status'    => 1,
            ],
            [
                'code'  => 'en',
                'name'  => 'English',
                'charset'   => 'UTF-8',
                'order'     => 2,
                'status'    => 1,
            ],
            [
                'code'  => 'de',
                'name'  => 'Deutsch',
                'charset'   => 'UTF-8',
                'order'     => 3,
                'status'    => 1,
            ],
            [
                'code'  => 'ru',
                'name'  => 'русский',
                'charset'   => 'UTF-8',
                'order'     => 4,
                'status'    => 0,
            ],
        ];

        Language::insert( $arr );

        dump('# Diller oluşturuldu.');
    }
}
